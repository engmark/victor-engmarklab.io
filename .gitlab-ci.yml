---
# You can override the included template(s) by including variable overrides
# SAST customization:
# https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization:
# https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization:
# https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
default:
  image: nixos/nix:2.26.3@sha256:cf7393e408da5ad343dad43670be72d7ee062b2a6a687990e9613ef9dc8bf2f6

stages:
  - setup
  - test
  - build test
  - deploy

variables:
  CACHIX_CACHE_NAME: big
  LC_ALL: C.UTF-8

.cachix:
  before_script:
    - nix-env --install --attr nixpkgs.cachix
    - cachix use "$CACHIX_CACHE_NAME"

prepare Nix shell:
  extends: .cachix
  stage: setup
  script:
    - nix-shell --pure --run true
    - nix-store --query --references "$(nix-instantiate shell.nix)" | xargs
      nix-store --realise | xargs nix-store --query --requisites | cachix push
      "$CACHIX_CACHE_NAME"
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

lint contents:
  extends: .cachix
  stage: test
  script: nix-shell --pure --run 'pre-commit run --all-files'
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

lint commit messages:
  extends: .cachix
  stage: test
  variables:
    GIT_DEPTH: "" # Fetch all commits for gitlint
  script:
    - command=(gitlint
      --commits="origin/${CI_DEFAULT_BRANCH}..origin/${CI_COMMIT_REF_NAME}"
      --debug --fail-without-commits)
    - nix-shell --pure --run "${command[*]}"
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

test JavaScript:
  extends: .cachix
  stage: test
  script: nix-shell --pure --run 'LC_MESSAGES=C TZ=UTC mocha'
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

test Nix shell with broken interpreter dependency:
  extends: .cachix
  stage: test
  script:
    - nix-shell --pure --run 'python --version'
      _includes/shell-broken-interpreter-dependency.nix
    - nix-shell --pure --run 'pytest --version'
      _includes/shell-broken-interpreter-dependency.nix
    - nix-shell --pure --run '! python -c "import pytest"'
      _includes/shell-broken-interpreter-dependency.nix
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

test Nix shell with fixed interpreter dependency:
  extends: .cachix
  stage: test
  script:
    - nix-shell --pure --run 'python -c "import pytest"'
      _includes/shell-fixed-interpreter-dependency.nix
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

test Nix shell with interpreter symlink:
  extends: .cachix
  stage: test
  script:
    - nix-shell --pure --run './python -m pytest --version'
      _includes/shell-with-interpreter-symlink.nix
    - rm python
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

.pages:
  extends: .cachix
  script: nix-shell --pure --run ./.gitlab/build.bash
  artifacts:
    paths:
      - public

staging:
  extends: .pages
  stage: test
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

staging-test:
  stage: build test
  dependencies:
    - staging
  script:
    nix-shell --keep LC_ALL --pure --run "htmlproofer --allow-missing-href
    --check-sri --checks=Favicon,Images,Links,Scripts --disable-external
    ./public"
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

staging-test-external:
  stage: build test
  dependencies:
    - staging
  script:
    nix-shell --keep LC_ALL --pure --run "htmlproofer --allow-missing-href
    --check-sri --checks=Favicon,Images,Links,Scripts ./public"
  when: manual

pages:
  extends: .pages
  stage: deploy
  environment:
    name: Production
    url: https://paperless.blog/
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml
