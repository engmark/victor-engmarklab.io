---
title: YouTube channel web feed bookmarklet
categories:
  - YouTube
  - bookmarklet
  - web feed
  - Atom
excerpt_separator: <!--more-->
---

tl;dr Drag →→→ [YouTube channel feed][1] ←←← to your bookmarks to get to the web
feed of the current channel.

{% capture raw_bookmarklet %}{% include youtube-channel-feed-bookmarklet.js -%}{% endcapture %}
[1]:javascript:{{ raw_bookmarklet | uri_escape }}

<!--more-->

Did you know YouTube has Atom web feeds for channels? That means you can follow
your favourite content creators _individually_ in your favourite feed reader,
rather than relying on the jumbled-up subscriptions page. Unfortunately, YouTube
doesn't mention this feed in the relevant pages, hence this bookmarklet. Just go
to a user, channel, or video page, and click the bookmarklet to go to the web
feed.

---

## Technical stuff

If you're interested in how this works, here's the code:

<!-- prettier-ignore-start -->
```javascript
{{ raw_bookmarklet }}
```
<!-- prettier-ignore-end -->
