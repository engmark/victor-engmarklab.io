---
title: Start test names with “should”
categories:
  - programming
  - software
  - test driven development
tags:
  - naming
  - testing
author: Victor Engmark
---

<p>
  The purpose of a test is not just to enforce some behaviour on the code under
  test. When the test fails it also should provide enough information to
  understand which behaviour failed, where it failed, and (at least
  superficially) <em>why</em> it failed. If the only output of a failing test is
  just a binary value like “FAIL”, that test is only giving one bit of
  information to the developer. A good test framework will also print the test
  name and a call stack. We can’t change the call stack much, at least not
  without changing the semantics of the test, and how to actually write the test
  itself is a whole other story, but what does a useful test name look like?
</p>

<p>
  A trick I learned from
  <a href="https://thinkfoo.wordpress.com/">Dave Hounslow</a>, a former
  colleague, is to <em>start test names with “should”¹.</em> This has a few
  advantages over <code>test [function name]</code>:
</p>
<ul>
  <li>
    It <em>removes redundancy,</em> because the function name should already be
    in the call stack.
  </li>
  <li>
    It is <em>falsifiable,</em> that is, a person reviewing the test can decide
    to which degree the name agrees with the actual test. For example, they
    could point out that
    <code>should replace children when updating instance</code> verifies that
    new children are added, but not that old children are removed.
  </li>
  <li>
    It <em>encourages testing one property of the function per test,</em> like
    <code>should apply discount when total cost exceeds 100 dollars</code>,
    <code>should create record for valid input</code>, and
    <code>should return error code 1 for unknown error</code>.
    <code>test [function name]</code> encourages testing everything the function
    does (branches, side effects, error conditions, etc.) in one test.
  </li>
  <li>
    It invites the developer to write something <em>human readable.</em> I
    usually find “test …” names to be clunky to read. This may just be bias
    after years of using this technique.
  </li>
  <li>
    It is <em>better than a comment</em> explaining what the test does, because
    the comment will not be shown when the test fails.
  </li>
</ul>
<p>
  ¹ Some frameworks require that test names are formatted in some special way,
  like starting with “test”, using snake case, camel case or other. I’m ignoring
  that part of the naming for brevity and to avoid focusing on a specific
  language.
</p>
