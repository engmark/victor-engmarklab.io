---
title: Easy PostgreSQL testing with pgTAP and Nix
categories:
  - Nix
  - pgTAP
  - PostgreSQL
  - testing
---

This article explains one way to get from zero to a `psql` shell inside a local
PostgreSQL instance with pgTAP enabled. It's based on the [Nix
shell template]({% post_url 2023-07-02-nix-shell-template %}) article.

## Intro

To recap, all you need is to install Nix
[install Nix](https://nixos.org/download.html), declare your environment in a
`shell.nix` file, and run `nix-shell` to enter the environment. We'll start from
the template `shell.nix` in the original article:

```nix
{% include shell-template.nix -%}
```

## Building it

(Feel free to [skip this part](#the-result) if you just want the thrilling
conclusion.)

How do we need to change the template to achieve this? We can split it into a
few easily manageable tasks. First, we'll need to install PostgreSQL itself.
That means changing the `packages` list to include `pkgs.postgresql`. Then we
need a small Bash script to set up and connect to the database:

1. Set up some directives to exit early in case of failure in any of the
   subsequent commands.
1. Configure an "exit trap" to stop the database and remove the
   files[^keep-files] when exiting the interactive database session below.
1. Create the database cluster using the `initdb` command.
1. Start PostgreSQL itself using the `pg_ctl` command.
1. Create a test database using the `createdb` command. This lets us work on a
   completely blank database, rather than an internal database.
1. Open an interactive shell within the test database using the `psql` command.

[^keep-files]:
    If you want to keep the database files around after stopping PostgreSQL,
    simply delete the `rm […]` line in the `cleanup` function.

The result so far:

```nix
{% include shell-psql.nix -%}
```

Now we just need one more ingredient, pgTAP itself. We need to install it in
such a way that the PostgreSQL package is aware of it, and then we need to
enable it on our test database.

## The result

```nix
{% include shell-pgtap.nix -%}
```

pgTAP is now available by simply running `nix-shell`! To verify, try running a
trivial test suite:

```postgresql
SELECT * FROM no_plan();
SELECT is(2, 2);
SELECT * FROM finish();
```

## Variants

As you can see, enabling other PostgreSQL extensions from
[nixpkgs](https://search.nixos.org/packages) is just a matter of adding two
lines: `postgresqlPackages.[…]` and the
`psql --command="CREATE EXTENSION […]" --dbname="$db_name" --host="$PGDATA"`
command.

It's also possible to run ancient versions of PostgreSQL by
[installing it from an older version of nixpkgs](https://lazamar.co.uk/nix-versions/?channel=nixpkgs-unstable&package=postgresql).
Click on any of the versions above to get detailed instructions.
