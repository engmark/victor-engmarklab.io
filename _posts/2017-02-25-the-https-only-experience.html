---
title: The HTTPS-only experience
categories:
  - communication
  - HTTP
  - security
  - web
tags:
  - HTTPS
  - privacy
  - TLS
author: Victor Engmark
---

<p>
  EFF recently announced that
  <strong
    >"<a
      href="https://www.eff.org/deeplinks/2017/02/were-halfway-encrypting-entire-web"
      >We're Halfway to Encrypting the Entire Web</a
    >."</strong
  >
  As a celebration of this achievement I've started an experiment:
  <strong
    >as of yesterday, no unencrypted HTTP traffic reaches this
    machine<sup>*</sup>.</strong
  >
</p>

<h2>Experience</h2>
<p>
  Even though
  <a href="https://letsencrypt.org/"
    >securing your website is easier than ever</a
  >
  it will take some time before everybody encrypts. But there are a bunch of
  sites which support both secure and insecure HTTP transfer, and there are a
  few tricks to tilt the scales and make the current experience better:
</p>
<ul>
  <li>
    The
    <strong
      ><a href="https://www.eff.org/https-everywhere">HTTPS Everywhere</a>
      browser extension</strong
    >
    ensures that you use HTTPS whenever possible on thousands of sites.
  </li>
  <li>
    <strong>Editing the URL</strong> to add "<code>https://</code>" at the start
    works for some sites. If you get the dreaded certificate error page make
    sure to report them, and only add an exception if … well, that subject is
    too big to get into here. Just don't.
  </li>
</ul>
<p>
  Many sites have excellent HTTPS support, and have enabled
  <strong>HTTP Strict Transport Security (HSTS).</strong> In short, if your
  browser knows that the domain supports HTTPS (by visiting it or the browser
  being installed with it) you can simply type the domain name in the URL field
  and the browser will default to fetching the secure version of the site. On
  the other end of the spectrum I can still visit sites which have no HTTPS
  support at all if I really need to by using <strong>Tor</strong>, which
  provides privacy but not integrity or authenticity.
</p>
<p>
  <strong><code>pacman</code> stopped working</strong> after setting this up. It
  turns out the package database is fetched using unencrypted HTTP by default,
  but it was easy to
  <a href="https://www.archlinux.org/mirrorlist/">generate a new list</a> of
  only HTTPS mirrors.
</p>
<p>
  <strong>Some sites have a strange HTTPS setup.</strong> The BBC only support
  HTTPS for the front page, which is just weird. Why go to all that trouble for
  1% of the gain? Other sites require authentication to access using HTTPS,
  possibly not realising that setting up HTTPS for everyone would be easier.
</p>
<p>
  My <strong>home router</strong> runs DD-WRT, and the web interface for it is
  only accessible by HTTP by default. This is easy to configure though.
</p>
<p>
  <strong>OCSP</strong> uses HTTP (at least in Firefox), since
  <a href="https://security.stackexchange.com/a/72573/1220"
    >the returned file signature has to be checked separately anyway</a
  >. So if I go to about:config, change <code>security.OCSP.require</code> to
  <code>true</code>, and visit a site I haven't seen for a while, I get an error
  message like this:
</p>
<blockquote>
  <p>
    An error occurred during a connection to example.com. The OCSP server
    experienced an internal error. Error code: SEC_ERROR_OCSP_SERVER_ERROR
  </p>
</blockquote>
<p>
  The solution is to either allow OCSP queries specifically or to allow HTTP to
  specific hosts.
  <a href="https://unix.stackexchange.com/q/349203/3645"
    >Let's see what can be done…</a
  >
</p>
<p>
  The <strong>Steam client</strong> uses insecure HTTP for both game updates and
  the store pages.
  <a href="https://gaming.stackexchange.com/q/302571/6297"
    >There doesn't seem to be any way to force it to use HTTPS</a
  >, so I have submitted
  <a href="https://steamcommunity.com/discussions/forum/10/135511259335166046/"
    >suggestion</a
  >
  to Valve using the official channel.
</p>
<p>
  These have been the only major hassles so far. The only other sites I really
  can't get to work over HTTPS are various hold-outs like Wikia and BBC.
</p>
<h2>Setup</h2>
<p>
  The change was a simple
  <a
    href="https://gitlab.com/engmark/root/-/commit/f208f7f721902e71c6c13775a5b524bba081ba90"
    >addition to my Puppet manifest</a
  >:
</p>
<pre>
firewall { '100 drop insecure outgoing HTTP traffic':
  chain  =&gt; 'OUTPUT',
  dport  =&gt; 80,
  proto  =&gt; tcp,
  action =&gt; reject,
}</pre
>
<p>The resulting rule:</p>
<pre>
$ sudo iptables --list-rules OUTPUT | grep ^-A
-A OUTPUT -p tcp -m multiport --dports 80 -m comment --comment "100 drop insecure outgoing HTTP traffic" -j REJECT --reject-with icmp-port-unreachable</pre
>

<h2>Post mortem, 2020-10-15</h2>
<p>
  After almost two and a half years I disabled this rule on 2019-08-09, for the
  following reasons (in decreasing order of importance):
</p>
<ol>
  <li>Most interesting sites have moved to HTTPS by now! Woo!</li>
  <li>
    The excellent HTTPS Everywhere extension achieves basically the same, with
    the option to disable it per site. This is in most cases<sup>†</sup> easier
    than switching to the Tor Browser.
  </li>
  <li>
    The Tor Browser would sometimes be inconvenient as a way to get to HTTP-only
    sites, since several hosting providers seem to be hostile to Tor. Some seem
    to simply block anything that looks like it comes from Tor, I guess out of
    fear of being hacked? Also, spending several minutes solving Google CAPTCHAs
    is excruciating.
  </li>
  <li>
    I had to maintain an exclusion list for OCSP lookups (chicken-and-egg
    problem, really) and Steam, which stubbornly refuse to move to HTTPS for
    game content delivery. I guess that'll have to wait until the first
    high-profile exploit enabled by the fact that package signatures don't
    guarantee
    <em>confidentiality,</em> meaning that any eavesdroppers can tell exactly
    what you are installing.
  </li>
  <li>
    I moved from Puppet to Nix, and I couldn't be assed to convert some of the
    ugliest Puppet code.
  </li>
</ol>

<p>All in all I consider it a success:</p>
<ul>
  <li>✓ It prodded me to ask Valve and others to move to HTTPS.</li>
  <li>✓ I learned a bit about HTTPS (OCSP).</li>
  <li>
    ✓✗ I learned more than I cared to about Puppet. Don't get me wrong, it's
    excellent for <em>additive</em> changes, but it can't hold a candle to Nix
    in terms of reproducibility.
  </li>
  <li>
    ✗ Using Tor Browser was marginally more painful than I had expected. Not
    because of any fault with the browser, but because of being blocked by
    website configurations and CAPTCHAs.
  </li>
</ul>

<p>
  <sup>*</sup> Technical readers will of course notice that the configuration
  simply blocks port 80, while HTTP can of course be served on any port. The
  configuration wasn't meant as a safeguard against absolutely every way
  unencrypted HTTP content could be fetched, but rather focused on the &gt;99.9%
  of the web which serves unencrypted content (if any) on port 80. I would be
  interested in any <em>easy</em> solutions for blocking unencrypted HTTP across
  the board.
</p>

<p>
  <sup>†</sup> Some sites for whatever reason seem to drop rather than reject
  traffic to port 443. Unfortunately that means I have to wait for Firefox to
  time out before getting the prompt to disable HTTPS Everywhere on that site.
  Other sites do something broken on HTTPS, like heaps of DreamHost sites which
  just shows a generic 404 page and others which have an admin interface there.
</p>
