---
title: JetBrains IDEA repository sync
categories: IDE sync
---

If you have a paid version of IDEA, you probably already know that you can
automatically sync your settings to _the currently active account._ That doesn't
cover all use cases though:

- You can't share settings with _another account,_ whether your own or someone
  else's.
- You can't keep track of how your preferences have _changed over time._
- If you want to edit the global settings manually you first have to _find
  them._

Happily IDEA provides a second way of syncing settings, using a custom
_[settings repository](https://www.jetbrains.com/help/idea/configure-project-settings.html#share-project-through-vcs)._
It's pretty easy to set up:

1. Create a Git repository (the rest of the instructions assumes you're using a
   free private repo on GitLab).
2. If necessary,
   [change the default branch to "master"](https://youtrack.jetbrains.com/issue/IDEA-248738).
3. Create a project access token
   ([if you can](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html))
   or
   [personal access token](https://gitlab.com/-/profile/personal_access_tokens)
   with `read_repository` and `write_repository` access.
4. In the File → Manage IDE Settings menu:
   1. _Disable_ syncing to JetBrains account.
   2. Set up syncing to repo with your normal GitLab username and the token
      generated above.
