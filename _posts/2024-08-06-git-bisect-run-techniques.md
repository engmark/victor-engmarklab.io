---
title: Git bisect run techniques
categories:
  - Git
  - debugging
excerpt_separator: <!--more-->
---

`git bisect run` lets us find the breaking commit in O(log(N)) time for N
commits, by doing a binary search through commits to determine the one which
broke things. It is extremely useful, but the thing which often takes a long
time is figuring out which command to use to reliably determine whether a commit
is good or bad. This article explains some techniques to help with this
task.<!--more-->[^caveats]

[^caveats]:
    This article assumes that you're using GNU tools. If that means nothing to
    you, then you are probably using them.

## Script file

Put anything but the most trivial command in a script file, for easier reading
and writing. This way we won't need to deal with an extra layer of complexity
and bugs. A quick template:

```bash
{% include bisect-template.bash -%}
```

This way, once we set up the commands, the Git bisect run is always just
`git bisect run ./bisect.bash`.

## Search for text in command output

`grep --quiet` is probably the simplest way to search through standard output of
a command for a value. But for maximum debugging potential we probably want to
[print the original output of the command as-is](https://unix.stackexchange.com/a/686008/3645)
(slightly modified for readability):

```bash
{% include bisect-stdout-search.bash -%}
```

The above means that the commit will be considered _good_ if any line of
standard output of `COMMAND` matches `REGEX`. Examples:

```bash
{% include bisect-stdout-search-examples.bash -%}
```

### Negating the exit code

To treat a commit as _bad_ if any line of standard output of `COMMAND` matches
`REGEX`[^negation]:

[^negation]:
    Normally we would just use `!` to negate the exit code of a command, but
    [this does not play nicely with `errexit`](https://www.shellcheck.net/wiki/SC2251).

```bash
{% include bisect-stdout-search-negation.bash -%}
```

Examples:

```bash
{% include bisect-stdout-search-negation-examples.bash -%}
```

### Search through standard error

To search through standard _error_ instead of standard output, we need to swap
standard error and standard output at the start and end of the pipeline, while
telling `tee` to use another file descriptor for the copy of standard
output[^extra-fd]:

[^extra-fd]:
    This can probably be simplified to not use file descriptor 4, but that's
    enough file descriptor fiddling for now.

```bash
{% include bisect-stderr-search.bash -%}
```

Examples:

```bash
{% include bisect-stderr-search-examples.bash -%}
```

## Interactive program outputs

Sometimes the output is coming from an interactive program. It's kind of tedious
to deal with shutting down the program manually for each commit, so let's kill
the program after some generous timeout, assuming that it would've had the time
to output what we were looking for if it was going to:

```bash
{% include bisect-interactive.bash -%}
```

Examples:

```bash
{% include bisect-interactive-examples.bash -%}
```

## All of the above

To reproduce
[this issue](https://github.com/NixOS/nixpkgs/issues/332561)[^failed-bisect] and
verify [the fix](https://github.com/NixOS/nixpkgs/pull/332614), we need to check
for a warning in the standard error of digiKam, a GUI program. This is the
resulting script, with some annotations:

[^failed-bisect]:
    This issue seems to have existed basically forever, because a bisect with
    the previous release of the repo didn't find a relevant commit.

```bash
{% include bisect-digikam-example.bash -%}
```

Verifying that the test failed before the fix:

```console
$ git checkout 14d2848c9251bc92c475ba3c0d379b5696c4e04b~1 # before fix
Previous HEAD position was […]
HEAD is now at b0baa71c8a72 […]
$ ./bisect.bash # Should show the error message
/nix/store/6g7i3267qdj5brdd3y5yp667r2vfzh90-digikam-8.4.0
qt.qpa.plugin: Could not find the Qt platform plugin "wayland" in ""
digikam.metaengine: ExifTool process cannot be started ( "" )
digikam.general: DK_PLUGIN_PATH env.variable detected. We will use it to load plugin...
digikam.facedb: Cannot found faces engine model "shapepredictor.dat"
digikam.facedb: Faces recognition feature cannot be used!
digikam.facedb: Cannot found faces engine DNN model "openface_nn4.small2.v1.t7"
digikam.facedb: Faces recognition feature cannot be used!
Path override failed for key base::DIR_APP_DICTIONARIES and path '/nix/store/6g7i3267qdj5brdd3y5yp667r2vfzh90-digikam-8.4.0/bin/qtwebengine_dictionaries'
Path override failed for key base::DIR_APP_DICTIONARIES and path '/nix/store/i9c1lffqmhm8ham6wapnbm015s2zyz25-qtwebengine-6.7.2/libexec/qtwebengine_dictionaries'
Path override failed for key base::DIR_APP_DICTIONARIES and path '/nix/store/i9c1lffqmhm8ham6wapnbm015s2zyz25-qtwebengine-6.7.2/libexec/qtwebengine_dictionaries'
kf.xmlgui: Unhandled container to remove :  Digikam::DigikamApp
$ echo $? # Should've failed
1
```

Verifying that the test succeeds after the fix:

```console
$ git checkout 14d2848c9251bc92c475ba3c0d379b5696c4e04b # after fix
Previous HEAD position was b0baa71c8a72 […]
HEAD is now at 14d2848c9251 digikam: wrap with exiftool
$ ./bisect.bash # Should *not* show the error message
/nix/store/qjllzrspb5dmm8yywr4rb54ffps41nah-digikam-8.4.0
qt.qpa.plugin: Could not find the Qt platform plugin "wayland" in ""
digikam.general: DK_PLUGIN_PATH env.variable detected. We will use it to load plugin...
digikam.facedb: Cannot found faces engine model "shapepredictor.dat"
digikam.facedb: Faces recognition feature cannot be used!
digikam.facedb: Cannot found faces engine DNN model "openface_nn4.small2.v1.t7"
digikam.facedb: Faces recognition feature cannot be used!
Path override failed for key base::DIR_APP_DICTIONARIES and path '/nix/store/qjllzrspb5dmm8yywr4rb54ffps41nah-digikam-8.4.0/bin/qtwebengine_dictionaries'
Path override failed for key base::DIR_APP_DICTIONARIES and path '/nix/store/i9c1lffqmhm8ham6wapnbm015s2zyz25-qtwebengine-6.7.2/libexec/qtwebengine_dictionaries'
Path override failed for key base::DIR_APP_DICTIONARIES and path '/nix/store/i9c1lffqmhm8ham6wapnbm015s2zyz25-qtwebengine-6.7.2/libexec/qtwebengine_dictionaries'
kf.xmlgui: Unhandled container to remove :  Digikam::DigikamApp
$ echo $? # Should've succeeded
0
```
