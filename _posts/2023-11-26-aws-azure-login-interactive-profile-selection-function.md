---
title: AWS Azure login interactive profile selection function
categories:
  - AWS
  - Azure
  - CLI
  - software
---

I've created a wrapper for `aws-azure-login` to let you interactively select the
profile you want before logging in. I have a bunch of profiles, I can never
remember what they are called, and some of them have short session lifetimes, so
this has been a lifesaver.

## Features

- Uses local variables to avoid polluting the namespace.
- Forwards any extra arguments (such as `--no-prompt`) to the underlying
  `aws-ozure-login` seamlessly.
- Lets you select from profiles without a `source_profile`.
- Logs in by running the original `aws-azure-login` command under the hood.
- Sets `AWS_PROFILE`.
- If there are any sub-profiles of the selected profile, it lets you select from
  them to set `AWS_PROFILE` after logging in. This is necessary at work, where
  we use sub-profiles to assume a different role from the login step.

## Dependencies

- [aws-azure-login](https://www.npmjs.com/package/aws-azure-login)
- [Bash](https://www.gnu.org/software/bash/)
- [RipGrep](https://github.com/BurntSushi/ripgrep) (but feel free to convert it
  to use GNU Grep)

## The function

```bash
{% include aws-azure-login.bash -%}
```

## Setup

1. Copy the function into your `~/.bashrc`.
2. Restart your shell.

## Usage

Run `aws-azure-login` and follow the instructions.
