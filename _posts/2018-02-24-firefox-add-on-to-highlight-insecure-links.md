---
title: Insecure Links Highlighter browser extension
categories:
  - Firefox
  - JavaScript
  - open source
tags:
  - addon
  - browser
  - extension
  - HTTP
  - HTTPS
  - plugin
  - security
  - web
author: Victor Engmark
---

[Insecure Links Highlighter](https://addons.mozilla.org/firefox/addon/insecure-links-highlighter/)
simply puts a thick red border around insecure links on all websites:

- <span style="border-color: red !important; border-style: solid !important; border-width: medium !important;">[http://example.org](#)</span>
- <span style="border-color: red !important; border-style: solid !important; border-width: medium !important;">[ftp://example.org](#)</span>
- <span style="border-color: red !important; border-style: solid !important; border-width: medium !important;">[Dynamic links](#)</span>

It’s intended for a couple of use cases:

- As an end user, discover which sites take security seriously.
  [HSTS](https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security) is not
  yet widely deployed, and untrustworthy ISPs can easily intercept an insecure
  connection, even if the first thing which would normally happen after
  connecting is a HTTPS redirect.
- As a web developer, easily check whether any of the links on the page you are
  working on are not secure.

## Options

Highlighting _dynamic_ links (links with JavaScript event handlers) is
available, but disabled by default. There’s nothing event handlers can do that
the site couldn’t already have done by the time you click. The only thing this
protects against in practice is the site sending you to the wrong URL, and most
users aren’t going to be comparing the link URL to the final URL that closely
(in particular because of how common redirects are).

The _border colour_ is also configurable, in case red is a bit much.

## Feedback

Which
[features or options](https://gitlab.com/engmark/insecure-links-highlighter/-/issues)
would you like to see?

[Updated 2020-10-15.]
