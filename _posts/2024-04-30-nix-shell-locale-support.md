---
title: Nix shell locale support
categories:
  - Nix
  - Linux
  - locale
---

If you need to do _anything_ to do with locales in a Nix shell, you'll want to
add the following to your environment:

```nix
LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
```

See for example
[this repo](https://gitlab.com/engmark/engmark.gitlab.io/-/blob/95595eca87d4274238cc97e0c094a68d77e0fda9/shell.nix#L35-35).
