---
title: Why open source is unpopular in business
categories:
  - open source
  - software
  - work
tags: []
author: Victor Engmark
---

<p>
  At work, I have noticed what seems like an inherent resistance against
  <acronym title="Free and open source software">FOSS</acronym>. I'll try to
  summarize what I believe are the main reasons for this.
</p>

<h4>Free lunch</h4>
<p>
  Firstly, There Ain't No Such Thing As A Free Lunch.<sup
    >[<a href="https://en.wikiquote.org/wiki/The_Moon_Is_a_Harsh_Mistress">1</a
    >]</sup
  >" In our consumer society, where you get what you pay for<sup
    >[<a href="https://en.wikiquote.org/wiki/Warren_Buffett">2</a>]</sup
  >, combining free and good is an oxymoron. Computer nerds never seem to accept
  the status quo, and have long provided proof against these statements<sup
    >[<a href="https://httpd.apache.org/">3</a>,
    <a href="https://www.linux.org/">4</a>]</sup
  >.
</p>
<p>
  It's also important to include just what non-technical end users mean by the
  terms "free" and "good". "Free as in freedom<sup
    >[<a href="https://www.oreilly.com/openbook/freedom/">5</a>]</sup
  >" is a godsend for the average Mr Fixit computer nerd, but end users couldn't
  care less. Code changes aside, I believe this is mainly because of lower
  expectations to technology; the status quo is always good enough. My family
  would happily use Word 6.0 today, limited features, incompatibilities, bugs,
  and crashes included. When DRM or format incompatibilities stop users from
  doing what they want, they will just stick with the tools which "work".
</p>
<p>
  "Good" in the eyes of the end user is very much different from the view of the
  typical nerd. End users won't read manuals (most of them are useless anyway),
  and don't want to know about the features which are not absolutely necessary.
  This is probably the reason for Microsoft setting "Hide unused menu items" as
  the default in the Office suite.
</p>
<h4>
  Publicity and <acronym title="Fear, Uncertainty and Doubt">FUD</acronym>
</h4>
<p>
  Since open source benefits from being as open as possible, all the flaws of
  the software is exposed to the world. If a single person somewhere has managed
  to provoke a bug in an open source application, no matter if the context and
  event sequence is completely singular, anyone interested can use the bug
  report as part of a reasoning for why the software is not ready for prime
  time, regardless of how many are already using it. On the other hand, for most
  (all?) closed source projects, there is nowhere to go to monitor the bug
  fixing process. Maybe it's fixed in the next release, maybe not. Maybe never.
</p>
<p>
  The complete openness of FOSS is like having a super high resolution video
  camera in your face, broadcasting live 24/7. It'll show every pimple, bruise,
  sneeze, and grimace, exposing every flaw for the world to see. For the
  burqa-wearing closed software, it's an easy target. Just wait until a major
  flaw is discovered, yell "See?!?", and don't mention that the flaw was fixed
  two days later.
</p>
<p>
  In addition, most open source projects rely on word of mouth for spreading,
  especially since their startup assets seldom include money at all*. This
  naturally propagates fastest among nerds, since they have no competition
  argument for not advertizing the latest and greatest to their fellows, and
  since any user would like as many others as possible to use the same as them,
  to speed up development.
</p>
<h4>Release procedures</h4>
<p>
  It is easy to see that open source projects care less for the hype of release
  numbers than their counterparts: While Firefox is currently at version 1, it's
  considered far superior to Internet Explorer 6 in such areas as security<sup
    >[<a
      href="https://usatoday30.usatoday.com/tech/news/computersecurity/2004-09-08-zombieinfect_x.htm"
      >6</a
    >]</sup
  >, feature set<sup
    >[<a
      href="https://www.forbes.com/2004/09/29/cx_ah_0929tentech.html?partner=tentech_newsletter&sh=539c5cf7876c"
      >7</a
    >]</sup
  >, and web standards support<sup
    >[8 (<em>Update: sorry, the link is dead</em>)]</sup
  >. Also, I don't think I've ever seen an open source software project which
  deviates from the standard "1.2[.3]" version numbering. This may at first seem
  like a bad thing for the end users, since they will have to test multiple
  applications if they really want to determine which one is the best. However,
  this is the only way to determine personal "goodness".
</p>
<p>
  *Notable examples to the contrary are Firefox and
  <a href="https://www.redhat.com/">RedHat Linux</a>.
</p>
