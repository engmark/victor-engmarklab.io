---
title: Rarotonga tourist tips
categories:
  - travel
  - Rarotonga
---

The Cook Islands recently opened up to tourists from New Zealand. Since the
COVID-19 situation meant we wouldn't be able to meet our family and friends
without something like four weeks of hotel quarantine, we decided instead to
take this first chance at an overseas experience in over a year. A friend
happened to have booked a vacation overlapping a couple days with ours, so after
arriving we met up for a chat. We learned a bunch of things about practicalities
on the island, and this is a collection of those I can remember plus a few more
we found out afterwards.

As always, things change, so make sure to verify if you rely on any of this
information.

## Preparation

Food is expensive, which shouldn't be a surprise on a small island with lots of
tourism. The choice is good, but not as good as a big New Zealand supermarket.
If you can't live without some specific food, make sure to bring it along.

Everywhere takes New Zealand dollars, but many places only take cash, so bring
some (I didn't use any ATMs). Everywhere which accepted plastic charged NZD, so
there were no extra fees withdrawing from a Kiwi bank account.

## Health

People burn food waste. It's not collected, and keeping it in or near the house
is a sure way to get ants and cockroaches. Find out where you're supposed to
chuck it, and don't mind the smoke.

Don't drink the tap water - ask where the nearest _free water station_ is. These
have potable water, and are spread out across the island. Other places are more
than happy to charge you for the same water.

## Food

Local produce is excellent. Bananas and pawpaw were my favourites.

Local produce is _sometimes_ cheap. For example, one of the main supermarkets
charged about 3x the price of another, and roadside stalls seem to be one of the
best alternatives: you take what you get, but what you get is great.

Coconut palms are everywhere, and you can eat the nuts off the ground (well, not
the whole thing, obviously). To get the outer shell off you can either cut it to
a taper using a machete (available in the supermarket and in kitchens) and then
lop the top off, or you can whack the nut onto a spike driven into the ground
and push the outer shell off that way, then carefully crack or puncture the
inner shell for some excellent water & flesh. The second method seems much safer
and faster, but I only learned about it too late to practice it. My naive early
attempts at splitting the nut down the middle is _much_ more effort, and it's
easy to lose all the coconut water if it cracks when you don't expect it to.

The "pre-shaven" coconuts for sale everywhere seem to be pretty young, so they
have almost no flesh and less sweet water.

Make a reservation before going to eat anywhere. There are lots of tourists, and
many places seem to be booked full every day.

## Money

Many places, including buses, take cash only.

A lot of businesses can be hard to reach: only sell on certain days, no opening
hours in the window, tiny signs, no sign down the road so you have time to react
the first time you see it, wrong phone number on the website, and no address/map
on the website are some antipatterns we've seen. Expect to have to ask around,
go twice to the same spot, or otherwise work around unexpected practices.

Wigmore's supermarket in Vaimaanga is, as far as I can tell, the only one open
24/7.

Everywhere has different opening hours. Some places are closed Sundays, Mondays,
Tuesdays, every second weekday, etc.

I've been told that New Zealand shops do _not_ take Cook Island dollars, so make
sure you consider that before returning.

## Transport

Traffic is leisurely, with 50 km/h being the max speed limit and most people
being careful drivers. Certainly _much_ more careful than kiwis.

Buses are every hour on the hour clockwise and every hour on the half hour
counter-clockwise. The counter-clockwise bus runs only until about 16:00, so
keep that in mind for returning late. I believe buses only take cash. Buying a
concession of 10 tickets ($30) is a lot cheaper than individual tickets ($5
each).

Hitchhiking is pretty common, and people will offer you a spot spontaneously if
you're out walking at night or in the rain.

Don't park under coconut palms! Once in a while you'll hear them thumping to the
ground, which is better than onto a windshield.

Bicycles are great; the circle road is flat and sealed but a little bumpy.

## Activities

Bring reef shoes and mask for snorkeling; corals kick back!

Most of the lagoon is shallow enough to stand up, but some areas have strong
currents. Look for signs.

Like in Welly, we couldn't find anywhere to sit and read/write for a whole day.
Cafés all close around 2-3 PM.

Check the rules if you want to bring any shells or other natural items home.

## Internet

Internet vouchers are generally expensive ($10 for 2.5 GB), but _some days_
there are $10 vouchers (bought in most stores) which give you _15 GB._ Basically
expect Vodafone to be out to get you.

Don't expect _free_ Wi-Fi anywhere, although most places have a Vodafone hotspot
where you can log in using a voucher.

Internet is slow, and drops out once in a while.
