antipatterns
api
atms
audiobooks
bookmarklet
breakpoint
commenters
crontab
cyclomatic
else's
encodings
fallbacks
filesystem
formatter
formatters
frobnicator
frobnicators
fullscreen
glyphs
gobbledygook
gzipped
hotspot
idempotence
inodes
isps
kanban
linter
linters
lookups
namespace
pragma
productionise
programmatically
readme
rebasing
redistributions
repo
reproducibility
runtime
schemas
snorkeling
submodules
symlink
toolchain
unmerged
unreferenced
welp
whitespace
