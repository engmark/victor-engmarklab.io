#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

vale --no-global --no-wrap "$@" | awk 'BEGIN {status = 1} 1; END {if(/^✔/) status = 0; exit(status)}'
