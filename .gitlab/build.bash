#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail
shopt -s failglob inherit_errexit

jekyll build

root_dir='public'
compressible_files_regex='.*\.\(css\|html\|ico\|js\|txt\|xml\)$'
find "$root_dir" -type f -regex "$compressible_files_regex" -exec gzip --force --keep {} \;
find "$root_dir" -type f -regex "$compressible_files_regex" -exec brotli --force --keep {} \;
