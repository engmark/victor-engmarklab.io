let
  pkgs = import (builtins.fetchTarball {
    name = "nixos-unstable-2024-12-26";
    url = "https://github.com/nixos/nixpkgs/archive/d70bd19e0a38ad4790d3913bf08fcbfc9eeca507.tar.gz";
    sha256 = "1968wcqnb5gb947js0kv17hy15nc5817ra66nj33nc532d342ig0";
  }) { };
  mocha = pkgs.nodePackages.mocha;
in
pkgs.mkShell {
  packages = [
    mocha
    pkgs.bashInteractive
    pkgs.cacert
    pkgs.check-jsonschema
    pkgs.editorconfig-checker
    pkgs.gitFull
    pkgs.gitlint
    pkgs.html-proofer
    pkgs.imagemagick
    pkgs.jekyll
    pkgs.nixf
    pkgs.nixfmt-rfc-style
    pkgs.nodePackages.prettier
    pkgs.nodejs
    pkgs.pre-commit
    pkgs.rubyPackages.ffi
    pkgs.rubyPackages.jekyll-favicon
    pkgs.rubyPackages.jekyll-feed
    pkgs.rubyPackages.jekyll-paginate
    pkgs.rubyPackages.jekyll-seo-tag
    pkgs.rubyPackages.jekyll-sitemap
    pkgs.rubyPackages.jekyll-webmention_io
    pkgs.shellcheck
    pkgs.shfmt
    pkgs.vale
    pkgs.yq-go
  ];
  env = {
    DICPATH = "${pkgs.hunspellDicts.en-gb-ise}/share/hunspell";
    LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
  };
  shellHook = ''
    ln --force --no-target-directory --symbolic "${mocha}/lib/node_modules/mocha" test/mocha
  '';
}
