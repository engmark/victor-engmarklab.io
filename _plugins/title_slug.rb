Jekyll::Hooks.register :posts, :pre_render do |post|
  title_slug = post.data['title'].downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')

  post.data['title_slug'] = title_slug
end
