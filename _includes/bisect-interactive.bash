{
    {
        timeout DURATION COMMAND || [[ $? -eq 124 ]]
    } |
        tee -p /dev/fd/3 |
        grep --quiet REGEX
} 3>&1
