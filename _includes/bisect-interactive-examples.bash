my_app() {
    echo 'stdout content'
    echo 'stderr content' >&2
    sleep infinity
}
export -f my_app # Propagate the function to the child `bash` shell

# Good commit because it finds "stdout"
{
    {
        timeout 0.5s bash -c my_app || [[ $? -eq 124 ]]
    } |
        tee -p /dev/fd/3 |
        grep --quiet stdout
} 3>&1

# Bad commit because "stderr" occurs on a different file descriptor
{
    {
        timeout 0.5s bash -c my_app || [[ $? -eq 124 ]]
    } |
        tee -p /dev/fd/3 |
        grep --quiet stderr
} 3>&1

# Bad commit because "other" does not appear anywhere
{
    {
        timeout 0.5s bash -c my_app || [[ $? -eq 124 ]]
    } |
        tee -p /dev/fd/3 |
        grep --quiet other
} 3>&1

# Prints "stderr content"
{
    {
        {
            timeout 0.5s bash -c my_app || [[ $? -eq 124 ]]
        } |
            tee -p /dev/fd/3 |
            grep --quiet stdout
    } 3>&1
} > /dev/null

# Prints "stdout content"
{
    {
        {
            timeout 0.5s bash -c my_app || [[ $? -eq 124 ]]
        } |
            tee -p /dev/fd/3 |
            grep --quiet stdout
    } 3>&1
} 2> /dev/null
