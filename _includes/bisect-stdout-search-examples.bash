my_command() {
    echo 'stdout content'
    echo 'stderr content' >&2
}

# Good commit because it finds "stdout"
{
    my_command |
        tee -p /dev/fd/3 |
        grep --quiet stdout
} 3>&1

# Bad commit because "stderr" occurs on a different file descriptor
{
    my_command |
        tee -p /dev/fd/3 |
        grep --quiet stderr
} 3>&1

# Bad commit because "other" does not appear anywhere
{
    my_command |
        tee -p /dev/fd/3 |
        grep --quiet other
} 3>&1

# Prints "stderr content"
{
    {
        my_command |
            tee -p /dev/fd/3 |
            grep --quiet stdout
    } 3>&1
} > /dev/null

# Prints "stdout content"
{
    {
        my_command |
            tee -p /dev/fd/3 |
            grep --quiet stdout
    } 3>&1
} 2> /dev/null
