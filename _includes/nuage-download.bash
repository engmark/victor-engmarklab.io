#!/usr/bin/env bash

set -o errexit -o nounset

if [[ $# -ne 1 ]]; then
    cat >&2 << EOF
$0: Download nuage image series, since they don't provide a convenient way of doing that.

Usage:

1. Log into your clinic's nuage patient portal.
2. Go to the first image in the series you want to download.
3. Open up a terminal side by side with the browser.
4. \`cd\` into your browser download directory, for example \`~/download\`. This directory should ideally be empty to help with validation at the end of this script.
5. Move the mouse pointer so it hovers above the middle of the download icon, but don't click it.
6. Run \`$0 IMAGE_COUNT\`. For example, \`$0 100\` will try to download 100 images.
EOF
    exit 2
fi

image_count="$1"

readonly pixels_from_download_button_to_link=40

image_index=0
while [[ "$image_index" -lt "$image_count" ]]; do
    ydotool click 0xC0                                                        # Left mouse button on download icon
    sleep 0.3s                                                                # Wait for menu to appear
    ydotool mousemove --xpos 0 --ypos "$pixels_from_download_button_to_link"  # Move down to "Download current image"
    ydotool click 0xC0                                                        # Left mouse button on "Download current image"
    sleep 0.3s                                                                # Wait for menu to disappear
    ydotool mousemove --xpos 0 --ypos -"$pixels_from_download_button_to_link" # Move back up to download icon
    ydotool key 108:1 108:0                                                   # Press and release down arrow
    ((++image_index))
done

# Check for missing files
for file_index in $(seq 1 "$image_count"); do
    if ! test -f ./*" ${file_index}.png"; then
        echo "Missing image #${file_index}" >&2
        exit_code=3
    fi
done

exit "${exit_code-0}"
