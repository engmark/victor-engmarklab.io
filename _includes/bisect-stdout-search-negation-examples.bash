my_command() {
    echo 'stdout content'
    echo 'stderr content' >&2
}

# Bad commit because it finds "stdout"
{
    if my_command | tee -p /dev/fd/3 | grep --quiet stdout; then
        false
    else
        true
    fi
} 3>&1

# Good commit because we're not searching through standard error
{
    if my_command | tee -p /dev/fd/3 | grep --quiet stderr; then
        false
    else
        true
    fi
} 3>&1

# Good commit because "other" does not appear anywhere
{
    if my_command | tee -p /dev/fd/3 | grep --quiet other; then
        false
    else
        true
    fi
} 3>&1
